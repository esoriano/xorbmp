# xorbmp

Simple C program to xor BMP images

# Build

```gcc -g -o xorbmp -Wall xorbmp.c```

# What?

This program encrypts the BMP pixmap using the Vernam cipher (One-Time Pad, OTP).
It writes a new BMP to the stdout. It's useful for creating examples and demos
(I use it in my lectures).

The second argument can be a random data file. If it's a BMP, both BMPs
must have the same metadata (resolution and color depth). See:

http://upload.wikimedia.org/wikipedia/commons/c/c4/BMPfileFormat.png

# Usage

```
$ ls -l Monte-Saint-Michel.bmp
-rw-rw-r-- 1 esoriano esoriano 30046586 Jun  5 12:51 Monte-Saint-Michel.bmp
$ dd if=/dev/urandom of=/tmp/k bs=30046586 count=1
1+0 records in
1+0 records out
30046586 bytes (30 MB, 29 MiB) copied, 0.150693 s, 199 MB/s
$ ./xorbmp Monte-Saint-Michel.bmp /tmp/k > /tmp/encrypted.bmp
$ ./xorbmp /tmp/encrypted.bmp /tmp/k > /tmp/clear.bmp
```

# Example: Why we can't reuse the OTP key

```
$ cd example
$ ./xorbmp lol.bmp key > C1.bmp
$ ./xorbmp loltext.bmp key > C2.bmp
$ ./xorbmp C1.bmp C2.bmp > C3.bmp
```

<center>
<figure class="image">
  <img src="example/example.png" alt="{{example}}">
 </figure>
</center>

so... DON'T DO IT
