#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

enum{
	Bufsz = 8*1024,
};

typedef struct Bmpmagic Bmpmagic;
struct Bmpmagic {
	unsigned char magic[2];
};

typedef struct Bmpheader Bmpheader;
struct Bmpheader {
	uint32_t	filesz;
	uint16_t	creator1;
	uint16_t	creator2;
	uint32_t	bmp_offset;
};

typedef struct Bmpinfo Bmpinfo;
struct Bmpinfo{
	uint32_t	header_sz;
	int32_t		width;
	int32_t		height;
	uint16_t	nplanes;
	uint16_t	bitspp;
	uint32_t	compress_type;
	uint32_t	bmp_bytesz;
	int32_t		hres;
	int32_t		vres;
	uint32_t	ncolors;
	uint32_t	nimpcolors;
};

void
readmetadata(FILE *f, Bmpmagic *mg, Bmpheader *hd, Bmpinfo *bmphd)
{
	if(fread(mg, sizeof(Bmpmagic), 1, f) != 1){
		fprintf(stderr, "error: read magic\n");
		exit(EXIT_FAILURE);
	}
	if(fread(hd, sizeof(Bmpheader), 1, f) != 1){
		fprintf(stderr, "error: read header\n");
		exit(EXIT_FAILURE);
	}
	if(fread(bmphd, sizeof(Bmpinfo), 1, f) != 1){
		fprintf(stderr, "error: read info\n");
		exit(EXIT_FAILURE);
	}
}

void
writemetadata(FILE *f, Bmpmagic *mg, Bmpheader *hd, Bmpinfo *bmphd)
{
	if(fwrite(mg, sizeof(Bmpmagic), 1, f) != 1){
		fprintf(stderr, "error: write magic\n");
		exit(EXIT_FAILURE);
	}
	if(fwrite(hd, sizeof(Bmpheader), 1, f) != 1){
		fprintf(stderr, "error: write header\n");
		exit(EXIT_FAILURE);
	}
	if(fwrite(bmphd, sizeof(Bmpinfo), 1, f) != 1){
		fprintf(stderr, "error: write info\n");
		exit(EXIT_FAILURE);
	}
}

void
transfer(FILE *in, FILE *out, long long len)
{
	char buf[Bufsz];
	int sz;
	int t = 0;

	while(t<len){
		sz = Bufsz;
		if(len-t < Bufsz){
			sz = len - t;
		}
		if(fread(buf, 1, sz, in) != sz){
			fprintf(stderr, "error: premature eof or read error\n");
			exit(EXIT_FAILURE);
		}
		if(fwrite(buf, 1, sz, stdout) != sz){
			fprintf(stderr, "error: fwrite failed\n");
			exit(EXIT_FAILURE);
		}
		t += sz;
	}
}

void
copymetadata(FILE *f, uint32_t *off, uint32_t *sz)
{
	Bmpmagic mg;
	Bmpheader hd;
	Bmpinfo bmphd;
	long long len;

	readmetadata(f, &mg, &hd, &bmphd);
	writemetadata(stdout, &mg, &hd, &bmphd);
	len =  hd.bmp_offset - sizeof(mg) - sizeof(hd) - sizeof(bmphd);
	transfer(f, stdout, len);
	*off = hd.bmp_offset;
	*sz = bmphd.bmp_bytesz;
}

void
copydata(FILE *f1, FILE *f2, uint32_t off, uint32_t sz)
{
	char c1;
	char c2;
	int i;

	if(fseek(f2, off, SEEK_SET) < 0){
		fprintf(stderr, "error: lseek\n");
		exit(EXIT_FAILURE);
	}
	/*
	 * Buffered I/O, we can read/write byte by byte
	 */
	for(i=0; i < sz;  i++){
		if(fread(&c1, 1, 1, f1) != 1){
			fprintf(stderr, "error: bitmap error\n");
			exit(EXIT_FAILURE);
		}
		if(fread(&c2, 1, 1, f2) != 1){
			fprintf(stderr, "error: bitmap error\n");
			exit(EXIT_FAILURE);
		}
		c1 ^= c2;
		if(fwrite(&c1, 1, 1, stdout) != 1){
			fprintf(stderr, "error: fwrite error\n");
			exit(EXIT_FAILURE);
		}
	}
	/*
	 * Copy the rest of the first file
	 */
	while(fread(&c1, 1, 1, f1) == 1){
		if(fwrite(&c1, 1, 1, stdout) != 1){
			fprintf(stderr, "error: fwrite\n");
			exit(EXIT_FAILURE);
		}
	}
	if(! feof(f1)){
		fprintf(stderr, "error: fread failed\n");
		exit(EXIT_FAILURE);
	}
}

int
main(int argc, char *argv[])
{
	FILE *f1;
	FILE *f2;
	uint32_t off;
	uint32_t sz;

	if(argc != 3){
		fprintf(stderr, "usage: xorbmp file1 file2\n");
		exit(EXIT_FAILURE);
	}
	f1 = fopen(argv[1], "r");
	if(f1 == NULL){
		fprintf(stderr, "error: can't open file %s\n", argv[1]);
		exit(EXIT_FAILURE);
	}
	f2 = fopen(argv[2], "r");
	if(f2 == NULL){
		fprintf(stderr, "error: can't open file %s\n", argv[2]);
		exit(EXIT_FAILURE);
	}
	copymetadata(f1, &off, &sz);
	copydata(f1, f2, off, sz);
	fclose(f1);
	fclose(f2);
	exit(EXIT_SUCCESS);
}
